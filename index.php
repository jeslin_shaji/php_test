<?php 

require_once 'model.php';
require_once 'controllers.php';
$uri = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
session_start();
if ('/index.php/' == $uri) {
    login_user();
} 
elseif ('/index.php/set_user/' == $uri) {
    set_user();
} 
elseif ('/index.php/submit_answer' == $uri) {
    check_answer();
} 
elseif ('/index.php/display_score' == $uri) {
    get_score();
} 
elseif ('/index.php/logout' == $uri) {
    session_destroy();
    header("Location:http://phptest.local/index.php/");
}

?>
