-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2017 at 03:31 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phptest`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE `candidate` (
  `id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `total_score` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `candidate`
--

INSERT INTO `candidate` (`id`, `email`, `total_score`) VALUES
(62, 'jeslin@gmail.com', 0),
(63, 'jeslin@gmail.com', 0),
(64, '', 0),
(65, '', 0),
(66, 'jeslin@gmail.com', 0),
(67, 'jeslin@gmail.com', 0),
(68, 'jeslin@gmail.com', 0),
(69, '', 0),
(70, 'jeslin@gmail.com', 0),
(71, 'jeslin@gmail.com', 0),
(72, 'jeslin@gmail.com', 0),
(73, 'jeslin@gmail.com', 0),
(74, 'jeslin@gmail.com', 0),
(75, 'jeslin@gmail.com', 0),
(76, 'jeslin@gmail.com', 0),
(77, 'jeslin@gmail.com', 0),
(78, 'jeslin@gmail.com', 0),
(79, 'jeslin@gmail.com', 0),
(80, 'jeslin@gmail.com', 0),
(81, 'jeslin@gmail.com', 0),
(82, 'jeslin@gmail.com', 0),
(83, 'jeslin@gmail.com', 0),
(84, 'jeslin@gmail.com', 0),
(85, 'jeslin@gmail.com', 0),
(86, 'jeslin@gmail.com', 0),
(87, 'jeslin@gmail.com', 0),
(88, 'jeslin@gmail.com', 0),
(89, '', 0),
(90, 'jes@yahoo.com', 0),
(91, '', 0),
(92, '', 0),
(93, '', 0),
(99, 'ghkjslk', 11),
(100, 'aaaaaaa', 0),
(101, 'hbgnjm', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `percentage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `percentage`) VALUES
(1, 'GK', 10),
(2, 'History', 5);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) NOT NULL,
  `question` varchar(255) NOT NULL,
  `option_a` varchar(200) NOT NULL,
  `option_b` varchar(200) NOT NULL,
  `option_c` varchar(200) NOT NULL,
  `option_d` varchar(200) NOT NULL,
  `answer` int(1) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `option_a`, `option_b`, `option_c`, `option_d`, `answer`, `category_id`) VALUES
(1, 'qn1', 'a', 'b', 'c', 'd', 1, 1),
(2, 'qn 2', 'a', 'b', 'c', 'd', 2, 2),
(3, 'qn 3', 'a', 'b', 'c', 'd', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE `score` (
  `id` int(10) NOT NULL,
  `part_id` int(10) NOT NULL,
  `qn_id` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `score`
--

INSERT INTO `score` (`id`, `part_id`, `qn_id`, `score`) VALUES
(1, 33, 1, 1),
(2, 44, 1, 1),
(3, 49, 3, 1),
(4, 63, 1, 1),
(5, 63, 1, 1),
(6, 63, 3, 1),
(7, 63, 1, 1),
(8, 63, 1, 1),
(9, 68, 2, 0),
(10, 68, 2, 0),
(11, 68, 2, 0),
(12, 69, 3, 0),
(13, 69, 3, 1),
(14, 70, 2, 1),
(15, 70, 3, 0),
(16, 71, 3, 0),
(17, 71, 3, 0),
(18, 71, 1, 0),
(19, 71, 2, 0),
(20, 74, 1, 0),
(21, 75, 2, 0),
(22, 76, 1, 0),
(23, 77, 3, 1),
(24, 78, 3, 1),
(25, 79, 1, 0),
(26, 79, 2, 0),
(27, 80, 3, 0),
(28, 80, 3, 1),
(29, 81, 2, 0),
(30, 82, 2, 0),
(31, 83, 1, 0),
(32, 83, 1, 1),
(33, 84, 3, 0),
(34, 85, 3, 0),
(35, 86, 1, 1),
(36, 87, 3, 1),
(37, 88, 3, 0),
(38, 90, 3, 1),
(39, 96, 2, 0),
(40, 96, 2, 0),
(41, 97, 2, 0),
(43, 0, 1, 1),
(44, 0, 1, 1),
(45, 0, 2, 1),
(46, 0, 3, 0),
(47, 0, 1, 0),
(48, 0, 3, 1),
(49, 0, 2, 1),
(50, 0, 3, 1),
(51, 0, 3, 1),
(52, 0, 3, 1),
(53, 0, 2, 1),
(54, 0, 1, 1),
(55, 0, 1, 1),
(56, 0, 1, 1),
(57, 0, 1, 1),
(63, 0, 1, 1),
(64, 0, 3, 1),
(65, 0, 3, 1),
(66, 101, 3, 1),
(67, 101, 1, 1),
(68, 101, 3, 1),
(69, 101, 3, 1),
(70, 101, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `time`
--

CREATE TABLE `time` (
  `id` int(11) NOT NULL,
  `no_qns` int(11) NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time`
--

INSERT INTO `time` (`id`, `no_qns`, `time`) VALUES
(1, 10, '10:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidate`
--
ALTER TABLE `candidate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time`
--
ALTER TABLE `time`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidate`
--
ALTER TABLE `candidate`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `score`
--
ALTER TABLE `score`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `time`
--
ALTER TABLE `time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
