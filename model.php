<?php

function open_database_connection() {
    $servername = 'localhost';
    $dbname = 'phptest';
    $username = 'root';
    $password = 'root';
    $link = new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
    return $link;
}

function close_database_connection() {
    $link = null;
}

function insert_user() {
    $link = open_database_connection();
    $query = "INSERT INTO candidate (`email`) VALUES (:email)";
    $result = $link->prepare($query);
    $result->bindParam(':email',$_POST['email']);
    $t = $result->execute();
    $lastid = $link->lastInsertId();
    $_SESSION['id'] = $lastid;
}
function get_time() {
    $link = open_database_connection();
    $query = "SELECT * FROM `time`";
    $result = $link->query($query);
    $result->execute();
    $row = $result->FETCH(PDO::FETCH_ASSOC);     
    return $row;
}
/*
function get_qn_id(){
    $link = open_database_connection();
    $query = "SELECT id FROM `questions`";
    $result = $link->query($query);
    $result->execute();
    while($row = $result->FETCH(PDO::FETCH_ASSOC)){
        $qnids[] = $row;
    }     
    return $qnids;
}
*/
/*
function get_max_id(){
    $link = open_database_connection();
    $query = "SELECT MAX(id) FROM `questions`";
    $result = $link->query($query);
    $result->execute();
    $row = $result->FETCH(PDO::FETCH_ASSOC)  
    return $row;
}
*/
function select_question() {
    $link = open_database_connection(); 
    $_SESSION['counter'] += 1;
    //$qnid = rand(1,5);
    
    $query1 = "SELECT id FROM questions WHERE id NOT IN(SELECT qn_id FROM score WHERE part_id=:part_id)";
    $result1 = $link->prepare($query1);
    $result1->bindParam(':part_id',$_SESSION['id']);
	$result1->execute();
	$qncount = 0;
    while($row1 = $result1->FETCH(PDO::FETCH_ASSOC)){
		$qnids[] = $row1;
		$qnidcount++;
	} 
	$qnno = $qnidcount-1;
	$randqn = rand(0, $qnno);
	$qnid = $qnids[$randqn]['id'];
    
    $query = "SELECT * FROM questions WHERE id=:id";
    $result = $link->prepare($query);
    $result->bindParam(':id', $qnid);
    $result->execute();
    $row = $result->FETCH(PDO::FETCH_ASSOC);
    return $row;
}

function enter_score($score) {
    $link = open_database_connection();
    $query = "INSERT INTO `score` (`part_id`, `qn_id`, `score`) VALUES (:part_id,:qn_id,:score)";
    $result = $link->prepare($query);
    $result->bindParam(':part_id',$_SESSION['id']);
    $result->bindParam(':qn_id',$_GET['qnid']);
    $result->bindParam(':score',$score);
    $result->execute();
}

function calculate_score() {
    $link = open_database_connection();
    $query = "SELECT COUNT(*) AS part_score FROM `score` WHERE part_id = :part_id AND score = 1";  
    $result = $link->prepare($query);
    $result->bindParam(':part_id',$_SESSION['id']);  
    $result->execute();
    $row = $result->FETCH(PDO::FETCH_ASSOC); 
    /*
    $query2 = "UPDATE candidate SET `total_score` = :total_score WHERE id = :id";
    $result2 = $link->prepare($query2);
    $result2->bindParam(':total_score',$row);
    $result2->bindParam(':id',$_SESSION['id']);  
    $result2->execute();
    */
    return $row;
}
?>